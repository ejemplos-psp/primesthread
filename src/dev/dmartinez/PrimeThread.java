package dev.dmartinez;

import java.math.BigInteger;

public class PrimeThread extends Thread {
	private int initialRange;
	private int finalRange;

	public PrimeThread(int initialRange, int finalRange) {
		this.initialRange = initialRange;
		this.finalRange = finalRange;
	}

	@Override
	public void run() {
		for (int i = initialRange; i < finalRange; i++) {
			if (isPrime(i)) {
				System.out.println(i);
			}
		}
	}

	public static boolean isPrime(int number) {
		BigInteger checkNumber = new BigInteger(String.valueOf(number));
		return checkNumber.isProbablePrime(1);
	}
}
