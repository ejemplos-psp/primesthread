package dev.dmartinez;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Cuantos hilos deseas utilizar: ");
		int nThreads = sc.nextInt();
		int numbers_per_thread = 100000 / nThreads;


		int auxInitialRange = 0;
		int auxFinalRange = 0;
		for (int i = 0; i < nThreads; i++) {
			auxInitialRange = numbers_per_thread * i;
			auxFinalRange = numbers_per_thread * (i +1);

			System.out.println("Thread-" + i + ": " + auxInitialRange + " -> " + auxFinalRange);

			new PrimeThread(auxInitialRange, auxFinalRange).start();

		}
	}
}
